#!/bin/bash


path=$PWD

cd $path/src/beescreens/admin/
npm i
cd $path/src/beescreens/play/
npm i
cd $path/src/beescreens/display/
npm i
cd $path/src/beescreens/api/
npm i
cd $path/test/beescreens/api/
npm i