# Organization

This page will describe how we organized ourselves to manage the project,
choices we made for the development cycles, etc.

# Meetings
We will meet every Thursday mornings, from 08:30 to 12:00, during the PDG course.

In addition to Thursday's meetings, we will meet every Tuesday, from 12:00 to 13:00 as well. This second meeting will allow us to check if everything goes well and
adapts the planning continuously.

# Minutes
On Thursday's meetings, we will take advantage to discuss the next steps of the
project rather than coding and working individually on our parts.

# Issues
Each created issue have three labels:

- Priority
- Status
- Type

This allows us to have an instant look at the state of an issue and where to
put the efforts.

# Milestones
We don't use "demo" labels in our issues. The main reason is because we prefer to
use milestones as a release target for the demos.

When creating an issue, we always set its milestone to know for what/when the issue
should be resolved.

This allows us to have an issue that can address multiple milestones rather to
have many labels to identify the issue as being applied for all milestones.

# Branches
We have protected the `master` branch to avoid any problems when working with git.

This allows us to be perfectly sure that the code on the `master` branch is always
sane.

We use branches and CI/CD to assure the quality of our code.

# Merge requests
We have set the merge requests to be very protective for our code:

- A merge can only be made if all regarding issues are closed.
- A merge can only be made if it has been approved by another member of the repository.
This ensure the code has been reviewed by another member before merging.
