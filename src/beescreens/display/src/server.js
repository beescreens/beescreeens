import io from 'socket.io-client';
import Peerjs from 'peerjs';
import bus from './event';
import store from './store';

class Socket {
    constructor() {
        this.socket = null;
        this.connected = false;
    }

    connect(url, jwt) {
        return new Promise((resolve, reject) => {
            this.socket = io.connect(url);
            this.socket.on('connect', () => {
                this.socket.emit('authentication', { jwt });
                this.socket.on('message', (data) => {
                    bus.$emit('message', data);
                });
                this.socket.on('authenticated', () => {
                    this.connected = true;
                    resolve();
                });
                this.socket.on('unauthorized', () => {
                    this.socket.close();
                    store.commit('connected', false);
                    reject();
                });
                this.socket.on('disconnect', () => {
                    this.disconnect();
                });
            });
        });
    }

    disconnect() {
        this.socket.close();
        this.connected = false;
    }
}

class Peer {
    constructor() {
        this.peer = null;
        this.connected = false;
    }

    connect(id, params) {
        return new Promise((resolve, reject) => {
            this.peer = new Peerjs(id, params);
            this.peer.on('open', () => {
                console.log('connected to peer!');
                this.connected = true;
                resolve();
            });

            this.peer.on('disconnected', () => {
                console.log('disconnected from peer');
                this.disconnect();
            });

            this.peer.on('error', (error) => {
                console.log('FAIL PEER');
                this.connected = false;
                this.disconnect();
                reject(error);
            });
        });
    }

    disconnect() {
        if (this.connected) this.peer.destroy();
        this.connected = false;
        store.commit('connected', false);
    }
}

export const socket = new Socket();
export const peer = new Peer();
