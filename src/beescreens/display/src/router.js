import Vue from 'vue';
import Router from 'vue-router';
import store from './store';
import Player from './views/Player.vue';

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: '/player',
            name: 'player',
            component: Player,
        },
        {
            path: '/',
            name: 'config',
            component: () => import('./views/Config.vue'),
        },
    ],
});

router.beforeEach((to, from, next) => {
    if (to.name !== 'config' && !store.getters.ready) {
        next({ name: 'config' });
    } else next();
});

export default router;
