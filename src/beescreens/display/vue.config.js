const path = require('path');

module.exports = {
    publicPath: './',
    outputDir: path.resolve(__dirname, '../dist/public/display'),

    lintOnSave: false,

    pluginOptions: {
        i18n: {
            locale: 'en',
            fallbackLocale: 'en',
            localeDir: 'locales',
            enableInSFC: false,
        },
    },
};
