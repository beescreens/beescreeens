export default {
    colors: {
        default: '#344675',
        // primary: "#42b883",
        primary: '#1d8cf8',
        info: '#1d8cf8',
        danger: '#fd5d93',
        teal: '#00d6b4',
        primaryGradient: ['transparent', 'transparent', 'transparent'],
    },
};
