import Vue from 'vue';
import VueMaterial from 'vue-material';
import App from './App';
import router from './router/index';

import BlackDashboard from './plugins/blackDashboard';
import i18n from './i18n';
import './assets/css/vue-material.min.css';

import { store } from './store';

Vue.use(BlackDashboard);
Vue.use(VueMaterial);

/* eslint-disable no-new */
new Vue({
    router,
    i18n,
    store,
    render: h => h(App),
}).$mount('#app');
