import DashboardLayout from '@/layout/dashboard/DashboardLayout.vue';

// GeneralViews
import NotFound from '@/pages/NotFoundPage.vue';

// Login page
import Login from '@/pages/Login.vue';

// Admin pages
import Dashboard from '@/pages/Dashboard.vue';
import Profile from '@/pages/Profile.vue';
import Notifications from '@/pages/Notifications.vue';
import Icons from '@/pages/Icons.vue';
import Maps from '@/pages/Maps.vue';
import Typography from '@/pages/Typography.vue';
import TableList from '@/pages/TableList.vue';
import Users from '@/pages/User/Users.vue';
import Displayers from '@/pages/Displayer/Displayers.vue';
import Applications from '@/pages/Application/Applications.vue';
import Players from '@/pages/Player/Players.vue';

import Options from '@/pages/Options/Options.vue';
import Sessions from '@/pages/Sessions/Sessions.vue';
import Queue from '@/pages/Queue/Queue.vue';

import Home from '@/pages/Home.vue';

const routes = [
    {
        path: '/login',
        component: Login,
    },
    {
        path: '/',
        component: DashboardLayout,
        redirect: '/home',
        children: [
            {
                path: 'dashboard',
                name: 'dashboard',
                component: Dashboard,
            },
            {
                path: 'profile',
                name: 'profile',
                component: Profile,
            },
            {
                path: 'notifications',
                name: 'notifications',
                component: Notifications,
            },
            {
                path: 'icons',
                name: 'icons',
                component: Icons,
            },
            {
                path: 'maps',
                name: 'maps',
                component: Maps,
            },
            {
                path: 'typography',
                name: 'typography',
                component: Typography,
            },
            {
                path: 'table-list',
                name: 'table-list',
                component: TableList,
            },
            {
                path: 'users',
                name: 'users',
                component: Users,
            },
            {
                path: 'displayers',
                name: 'displayers',
                component: Displayers,
            },
            {
                path: 'applications',
                name: 'applications',
                component: Applications,
            },
            {
                path: 'players',
                name: 'players',
                component: Players,
            },
            {
                path: 'options',
                name: 'options',
                component: Options,
            },
            {
                path: 'sessions',
                name: 'sessions',
                component: Sessions,
            },
            {
                path: 'queue',
                name: 'queue',
                component: Queue,
            },
            {
                path: 'home',
                name: 'home',
                component: Home,
            },
        ],
    },
    { path: '*', component: NotFound },
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};* */

export default routes;
