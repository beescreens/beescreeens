// eslint-disable-next-line
import Vue from 'vue';
// eslint-disable-next-line
import VueRouter from 'vue-router';
import routes from './routes';
import { userService } from '../helpers/user.services';

Vue.use(VueRouter);

// configure router
const router = new VueRouter({
    routes, // short for routes: routes
    // mode: 'history',
    linkExactActiveClass: 'active',
    scrollBehavior: (to) => { // Xavier : What is it ?
        if (to.hash) {
            return { selector: to.hash };
        }
        return { x: 0, y: 0 };
    },
});

router.beforeEach((to, from, next) => {
    
    // Public pages
    const publicPages = ['/login'];

    const isPublicPages = publicPages.includes(to.path);

    const loggedIn = localStorage.getItem('user');

    if (loggedIn && isPublicPages) {
        return next('/home');
    }

    if (!isPublicPages && !loggedIn) {
        return next('/login');
    }

    // Check if the token is the same on the server
    /* if (loggedIn && !isPublicPages) {
        userService.checkToken().then((value) => {
            if (!value) {
                return next('/login');
            }
        });
    } else if (!isPublicPages && !loggedIn) {
        return next('/login');
    } */

    next();
});

export default router;
