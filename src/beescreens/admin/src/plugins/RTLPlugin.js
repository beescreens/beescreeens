export default {
    install(Vue) {
        const app = new Vue({
            data() {
                return {
                    isRTL: false,
                };
            },
            methods: {
                getDocClasses() {
                    // eslint-disable-next-line
                    return document.body.classList;
                },
                enableRTL() {
                    this.isRTL = true;
                    this.getDocClasses().add('rtl');
                    this.getDocClasses().add('menu-on-right');
                    this.toggleBootstrapRTL(true);
                },
                disableRTL() {
                    this.isRTL = false;
                    this.getDocClasses().remove('rtl');
                    this.getDocClasses().remove('menu-on-right');
                    this.toggleBootstrapRTL(false);
                },
                toggleBootstrapRTL(value) {
                    // eslint-disable-next-line
                    for (let i = 0; i < document.styleSheets.length; i++) {
                        // eslint-disable-next-line
                        const styleSheet = document.styleSheets[i];
                        const { href } = styleSheet;
                        if (href && href.endsWith('bootstrap-rtl.css')) {
                            styleSheet.disabled = !value;
                        }
                    }
                },
            },
        });

        // eslint-disable-next-line
        Vue.prototype.$rtl = app;
    },
};
