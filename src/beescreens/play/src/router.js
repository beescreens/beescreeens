import Vue from 'vue';
import Router from 'vue-router';
import apps from './AppLoader';
import Menu from './views/Menu.vue';

Vue.use(Router);

const routes = [
    {
        path: '/',
        name: 'menu',
        component: Menu,
    },
    {
        path: '/queue',
        name: 'queue',
        component: () => import(/* webpackChunkName = "queue" */ './views/Queue.vue'),
    },
];

apps.forEach((details, key) => {
    routes.push({
        path: `/app/${key}`,
        name: key,
        component: details.root,
    });
});

export default new Router({
    routes,
});
