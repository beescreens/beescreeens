const path = require('path');

module.exports = {
    publicPath: './',
    outputDir: path.resolve(__dirname, '../dist/public/play'),

    configureWebpack: {
        resolve: {
        },
    },
};
