const envFile = require('find-config')('.env');
require('dotenv').config({ path: envFile });

const container = require('./container');

const {
    adminUsername,
    adminPassword,
    database,
    ioServer,
    peerServer,
    restServer,
    UserService,
} = container.cradle;

// Grafully stops the server on stop
process.on('SIGINT', () => {
    peerServer.on('stopped', () => {
        // Hack - see peerServer.stop()
        process.exit(0);
    });

    ioServer.on('stopped', () => {
        peerServer.stop();
    });

    database.on('disconnected', () => {
        ioServer.stop();
    });

    restServer.on('stopped', () => {
        database.disconnect();
    });

    restServer.stop();
});

// Start the server when database is connected
database.on('connected', () => {
    UserService
        .createUser(adminUsername, adminPassword, 'admin')
        .catch(() => true)
        .finally(() => {
            restServer.start();
            ioServer.start();
            peerServer.start();
        });
});

// Start the database
database.connect();
