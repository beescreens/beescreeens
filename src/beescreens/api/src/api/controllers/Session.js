const container = require('../../container');

const {
    SessionDTO,
    SessionService,
    Response200,
    Response204,
} = container.cradle;

module.exports.deleteSession = function deleteSession(req, res) {
    let { sessionId } = req.swagger.params;

    sessionId = sessionId.value;

    SessionService.deleteSession(sessionId)
        .then(() => {
            Response204.send(res);
        });
};

module.exports.getSession = function getSession(req, res) {
    let { sessionId } = req.swagger.params;

    sessionId = sessionId.value;

    SessionService.getSession(sessionId)
        .then((session) => {
            const dto = new SessionDTO(session);

            Response200.send(dto, res);
        });
};

module.exports.getSessions = function getSessions(req, res) {
    SessionService.getSessions()
        .then((sessions) => {
            const sessionsDTO = [];

            sessions.forEach((session) => {
                sessionsDTO.push(new SessionDTO(session));
            });

            Response200.send(sessionsDTO, res);
        });
};

module.exports.updateSession = function updateSession(req, res) {
    let { sessionId, session } = req.swagger.params;

    sessionId = sessionId.value;
    session = session.value;

    SessionService.updateSession(sessionId, session.displayerName)
        .then(() => {
            Response204.send(res);
        });
};
