const container = require('../../container');

const {
    EntityTypes,
    DontAcceptPlayersError,
    OptionService,
    PlayService,
    JwtService,
    Response201,
    Response403,
    Response404,
} = container.cradle;

module.exports.playApp = function playApp(req, res) {
    // We get the current defined options
    const acceptNewPlayers = JSON.parse(OptionService.getOptionValue('accept_new_players'));

    // Get the player's details from the request
    let { applicationName } = req.swagger.params;

    applicationName = applicationName.value;

    if (acceptNewPlayers) {
        PlayService.play(applicationName)
            .then((username) => {
                JwtService.sign({
                    role: EntityTypes.PLAYER.toString(),
                    username,
                    applicationName,
                }).then((token) => {
                    Response201.send({
                        id: username.replace(/[^A-Za-z0-9]/g, ''),
                        jwt: token,
                    }, res);
                });
            })
            .catch(() => {
                Response404.send(res);
            });
    } else {
        Response403.send(DontAcceptPlayersError.message, res);
    }
};
