const container = require('../../container');

const {
    ApplicationDTO,
    ApplicationService,
    Response200,
    Response204,
    Response404,
} = container.cradle;

module.exports.deleteApp = function deleteApp(req, res) {
    let { appName } = req.swagger.params;

    appName = appName.value;

    ApplicationService.deleteApp(appName)
        .then(() => {
            Response204.send(res);
        })
        .catch(() => {
            Response404.send(res);
        });
};

module.exports.getApp = function getApp(req, res) {
    let { applicationName } = req.swagger.params;

    applicationName = applicationName.value;

    ApplicationService.getApp(applicationName)
        .then((app) => {
            const dto = new ApplicationDTO(app);

            Response200.send(dto, res);
        })
        .catch(() => {
            Response404.send(res);
        });
};

module.exports.getApps = function getApps(req, res) {
    ApplicationService.getApps()
        .then((applications) => {
            const applicationsDTO = [];

            applications.forEach((application) => {
                applicationsDTO.push(new ApplicationDTO(application));
            });

            Response200.send(applicationsDTO, res);
        });
};

module.exports.updateApp = function updateApp(req, res) {
    let { applicationName, application } = req.swagger.params;

    applicationName = applicationName.value;
    application = application.value;

    ApplicationService.updateApp(applicationName, application.status)
        .then(() => {
            Response204.send(res);
        })
        .catch(() => {
            Response404.send(res);
        });
};
