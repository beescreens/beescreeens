const container = require('../../container');

const {
    PlayerDTO,
    PlayerService,
    Response200,
    Response204,
    Response404,
} = container.cradle;

module.exports.getPlayer = function getPlayer(req, res) {
    let { username } = req.swagger.params;

    username = username.value;

    PlayerService.getPlayer(username)
        .then((player) => {
            const dto = new PlayerDTO(player);

            Response200.send(dto, res);
        })
        .catch(() => {
            Response404.send(res);
        });
};

module.exports.getPlayers = function getPlayers(req, res) {
    PlayerService.getPlayers()
        .then((players) => {
            const playersDTO = [];

            players.forEach((player) => {
                playersDTO.push(new PlayerDTO(player));
            });

            Response200.send(playersDTO, res);
        });
};

module.exports.kickPlayer = function kickPlayer(req, res) {
    let { username } = req.swagger.params;

    username = username.value;

    PlayerService.kickPlayer(username)
        .then(() => {
            Response204.send(res);
        })
        .catch(() => {
            Response404.send(res);
        });
};

module.exports.updatePlayer = function updatePlayer(req, res) {
    let { username, player } = req.swagger.params;

    username = username.value;
    player = player.value;

    PlayerService.updatePlayer(username, player.status)
        .then(() => {
            Response204.send(res);
        })
        .catch(() => {
            Response404.send(res);
        });
};
