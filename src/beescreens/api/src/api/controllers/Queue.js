const container = require('../../container');

const {
    QueueElementDTO,
    QueueService,
    Response200,
    Response204,
} = container.cradle;

module.exports.deleteQueueElement = function deleteQueueElement(req, res) {
    let { position } = req.swagger.params;

    position = position.value;

    QueueService.deleteQueueElement(position)
        .then(() => {
            Response204.send(res);
        });
};

module.exports.updateQueueElement = function updateQueueElement(req, res) {
    let { position, queueElement } = req.swagger.params;

    position = position.value;
    queueElement = queueElement.value;

    QueueService.updateQueueElement(position, queueElement.position)
        .then(() => {
            Response204.send(res);
        });
};


module.exports.getQueueElement = function getQueueElement(req, res) {
    let { queueElementPosition } = req.swagger.params;

    queueElementPosition = queueElementPosition.value;

    QueueService.getQueueElement(queueElementPosition)
        .then((queueElement) => {
            const dto = new QueueElementDTO(queueElement);

            Response200.send(dto, res);
        });
};

module.exports.getQueueElements = function getQueueElements(req, res) {
    QueueService.getQueueElements()
        .then((queueElements) => {
            const queueElementsDTO = [];

            queueElements.forEach((queueElement) => {
                queueElementsDTO.push(new QueueElementDTO(queueElement));
            });

            Response200.send(queueElementsDTO, res);
        });
};
