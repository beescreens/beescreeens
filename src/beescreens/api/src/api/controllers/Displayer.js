const container = require('../../container');

const {
    DisplayerDTO,
    DisplayerService,
    JwtService,
    EntityTypes,
    OptionService,
    DontAcceptDisplayersError,
    OnlyAcceptKnownDisplayersError,
    OnlyAcceptNewDisplayersError,
    PasswordIsIncorrectError,
    PasswordIsRequiredError,
    Response200,
    Response201,
    Response204,
    Response403,
    Response404,
} = container.cradle;

module.exports.deleteDisplayer = function deleteDisplayer(req, res) {
    let { displayerName } = req.swagger.params;

    displayerName = displayerName.value;

    DisplayerService.deleteDisplayer(displayerName)
        .then(() => {
            Response204.send(res);
        })
        .catch(() => {
            Response404.send(res);
        });
};

module.exports.getDisplayer = function getDisplayer(req, res) {
    let { displayerName } = req.swagger.params;

    displayerName = displayerName.value;

    DisplayerService.getDisplayer(displayerName)
        .then((displayer) => {
            const dto = new DisplayerDTO(displayer);

            Response200.send(dto, res);
        })
        .catch(() => {
            Response404.send(res);
        });
};

module.exports.getDisplayers = function getDisplayers(req, res) {
    DisplayerService.getDisplayers()
        .then((displayers) => {
            const displayersDTO = [];

            displayers.forEach((displayer) => {
                displayersDTO.push(new DisplayerDTO(displayer));
            });

            Response200.send(displayersDTO, res);
        });
};

module.exports.registerDisplayer = function registerDisplayer(req, res) {
    // We get the current defined options
    const acceptNewDisplayers = JSON.parse(OptionService.getOptionValue('accept_new_displayers'));
    const acceptKnownDisplayers = JSON.parse(OptionService.getOptionValue('accept_known_displayers'));
    const displayersPassword = OptionService.getOptionValue('displayers_password');
    const requirePassword = displayersPassword.length !== 0;

    // Get the displayer's details from the request
    let { displayer } = req.swagger.params;

    displayer = displayer.value;

    const {
        name,
        location,
        width,
        height,
        password,
    } = displayer;

    let isKnown;

    DisplayerService.getDisplayer(name)
        .then(() => {
            isKnown = true;
        }).catch(() => {
            isKnown = false;
        }).finally(() => {
            // Check if the displayer is accepted or not
            let needToCreate = false;
            let errors = true;

            if (
                acceptKnownDisplayers
                && isKnown
            ) {
                // We don't accept new displayers,
                // but we accept known displayers
                // and it's known.
                needToCreate = false;
                errors = false;
            } else if (
                acceptNewDisplayers
                && !acceptKnownDisplayers
                && !isKnown
                && !requirePassword
            ) {
                // We accept new displayers,
                // but don't accept known displayers
                // and it's not known
                // and we don't require a password
                needToCreate = true;
                errors = false;
            } else if (
                acceptNewDisplayers
                && !acceptKnownDisplayers
                && !isKnown
                && requirePassword
                && password != null
                && password === displayersPassword
            ) {
                // We accept new displayers,
                // but don't accept known displayers,
                // and it's not known
                // and we require a password
                // and the password is not null
                // and the password is correct
                needToCreate = true;
                errors = false;
            } else if (
                acceptNewDisplayers
                && acceptKnownDisplayers
                && isKnown
                && requirePassword
                && password != null
                && password === displayersPassword
            ) {
                // We accept new displayers,
                // and we accept known displayers,
                // and it's known
                // and we require a password
                // and the password is not null
                // and the password is correct
                needToCreate = false;
                errors = false;
            } else if (
                acceptNewDisplayers
                && acceptKnownDisplayers
                && !isKnown
                && requirePassword
                && password != null
                && password === displayersPassword
            ) {
                // We accept new displayers,
                // and we accept known displayers,
                // and it's not known
                // and we require a password
                // and the password is not null
                // and the password is correct
                needToCreate = true;
                errors = false;
            } else if (
                acceptNewDisplayers
                && acceptKnownDisplayers
                && isKnown
                && !requirePassword
            ) {
                // We accept new displayers,
                // and we accept known displayers,
                // and it's known
                // and no password is required
                needToCreate = false;
                errors = false;
            } else if (
                acceptNewDisplayers
                && acceptKnownDisplayers
                && !isKnown
                && !requirePassword
            ) {
                // We accept new displayers,
                // and we accept known displayers,
                // and it's not known
                // and no password is required
                needToCreate = true;
                errors = false;
            }

            if (errors) {
                const response = {};

                if (
                    !acceptNewDisplayers
                    && !acceptKnownDisplayers
                ) {
                    // We don't accept new displayers,
                    // and we don't accept known displayers
                    response.message = DontAcceptDisplayersError.message;
                } else if (
                    !acceptNewDisplayers
                    && acceptKnownDisplayers
                    && !isKnown
                ) {
                    // We don't accept new displayers,
                    // and we accept known displayers,
                    // and it's not known
                    response.message = OnlyAcceptKnownDisplayersError.message;
                } else if (
                    acceptNewDisplayers
                    && !acceptKnownDisplayers
                    && isKnown
                ) {
                    // We accept new displayers,
                    // and we don't accept known displayers,
                    // and it's known
                    response.message = OnlyAcceptNewDisplayersError.message;
                } else if (
                    acceptNewDisplayers
                    && !acceptKnownDisplayers
                    && !isKnown
                    && requirePassword
                    && password == null
                ) {
                    // We accept new displayers,
                    // and we don't accept known displayers,
                    // and it's not known
                    // and we required a password
                    // and password is null
                    response.message = PasswordIsRequiredError.message;
                    response.requirePassword = true;
                } else if (
                    acceptNewDisplayers
                    && !acceptKnownDisplayers
                    && !isKnown
                    && requirePassword
                    && password != null
                    && password !== displayersPassword
                ) {
                    // We accept new displayers,
                    // and we don't accept known displayers,
                    // and it's not known
                    // and we required a password
                    // and password is not null
                    // and the password is incorrect
                    response.message = PasswordIsIncorrectError.message;
                    response.requirePassword = true;
                }

                Response403.send(response, res);
            } else if (needToCreate) {
                DisplayerService.registerDisplayer(name, location, width, height)
                    .then(() => JwtService.sign({
                        role: EntityTypes.DISPLAYER.toString(),
                        displayerName: name,
                    }))
                    .then((token) => {
                        Response201.send({
                            id: name.replace(/[^A-Za-z0-9]/g, ''),
                            jwt: token,
                        }, res);
                    });
            } else {
                Response204.send(res);
            }
        });
};
module.exports.updateDisplayer = function updateDisplayer(req, res) {
    let { displayerName, displayer } = req.swagger.params;

    displayerName = displayerName.value;
    displayer = displayer.value;

    DisplayerService.updateDisplayer(
        displayerName,
        displayer.width,
        displayer.height,
        displayer.status,
    )
        .then(() => {
            Response204.send(res);
        })
        .catch(() => {
            Response404.send(res);
        });
};
