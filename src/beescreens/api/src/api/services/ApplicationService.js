const fs = require('fs');
const path = require('path');
const childProcess = require('child_process');
const ip = require('ip');

module.exports = class {
    constructor(ApplicationModel, Status, debug, applicationsPath) {
        const applications = new Map();

        const appsDirectories = fs
            .readdirSync(applicationsPath)
            .filter(file => fs.statSync(path
                .resolve(applicationsPath, file))
                .isDirectory())
            .map(directory => path
                .resolve(applicationsPath, directory));

        appsDirectories
            .forEach((appDirectory) => {
                // Get the manifest
                const manifest = JSON.parse(
                    fs.readFileSync(
                        path.join(
                            appDirectory, './Manifest.json',
                        ),
                    ),
                );

                // Install dependencies
                manifest.install.commands.forEach((command) => {
                    childProcess.exec(`
                        root=$PWD && \
                        cd ${appDirectory} && \
                        ${command} && \
                        cd root
                    `);
                });

                // Instanciate the application
                // eslint-disable-next-line global-require, import/no-dynamic-require
                const Application = require(
                    path.join(
                        appDirectory, './server/App.js',
                    ),
                );

                const instance = new Application();

                instance.install({
                    peerServer: {
                        host: ip.address(),
                        port: 8080,
                        path: '/api/peers',
                    },
                })
                    .then(() => {
                        // Create the application model
                        const application = new ApplicationModel(
                            manifest.name,
                            manifest.logo,
                            manifest.description,
                            manifest.version,
                            manifest.homepage,
                            manifest.authors,
                            instance,
                            Status.ACTIVE.toString(),
                        );

                        // Store the application
                        applications.set(application.name, application);

                        debug(`Application '${manifest.name}' has been successfully installed.`);
                    })
                    .catch(() => {
                        debug(`Application '${manifest.name} cannot be installed.`);
                    });
            });

        this.ApplicationModel = ApplicationModel;
        this.applications = applications;
        this.debug = debug('beescreens:application-service');
    }

    deleteApp(applicationName) {
        const { applications, debug } = this;

        return new Promise((resolve, reject) => {
            this.getApp(applicationName)
                .then(application => application.instance.uninstall())
                .then(() => {
                    applications.delete(applicationName);

                    debug(`Application ${applicationName} has been successfully deleted.`);
                    resolve();
                })
                .catch(() => {
                    reject();
                });
        });
    }

    getApp(applicationName) {
        const { applications } = this;

        return new Promise((resolve, reject) => {
            const app = applications.get(applicationName);

            if (app != null) {
                resolve(app);
            } else {
                reject();
            }
        });
    }

    getApps() {
        const { applications } = this;

        return new Promise((resolve) => {
            resolve([...applications.values()]);
        });
    }

    updateApp(applicationName, status) {
        const { applications } = this;

        return new Promise((resolve, reject) => {
            this.getApp(applicationName)
                .then((application) => {
                    // eslint-disable-next-line no-param-reassign
                    application.status = status;

                    applications.set(applicationName, application);

                    resolve();
                })
                .catch(() => reject());
        });
    }
};
