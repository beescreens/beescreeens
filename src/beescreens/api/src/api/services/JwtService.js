// TODO - is this good practice in DI
const jsonwebtoken = require('jsonwebtoken');

module.exports = class {
    constructor(JwtModel, jwtSecret, jwtExpiration) {
        this.JwtModel = JwtModel;
        this.jwtSecret = jwtSecret;
        this.jwtExpiration = jwtExpiration;

        const knownJwts = new Map();
        const blacklistedJwts = new Map();

        this.knownJwts = knownJwts;
        this.blacklistedJwts = blacklistedJwts;
    }

    sign(payload) {
        const { jwtExpiration, jwtSecret } = this;

        return new Promise((resolve) => {
            const json = JSON.stringify(payload);
            const plainObject = JSON.parse(json);

            const token = jsonwebtoken.sign(plainObject, jwtSecret, {
                expiresIn: jwtExpiration,
            });

            this.addKnownJwt(token);

            resolve(token);
        });
    }

    verify(token) {
        const { jwtSecret } = this;

        return new Promise((resolve, reject) => {
            this.isJwtBlacklisted(jwtSecret)
                .then(() => reject())
                .catch(() => {
                    jsonwebtoken.verify(token, jwtSecret, (err, decodedToken) => {
                        if (err != null) {
                            reject();
                        }

                        resolve(decodedToken);
                    });
                });
        });
    }

    getKnownJwts() {
        const { knownJwts } = this;

        return new Promise((resolve) => {
            resolve(knownJwts);
        });
    }

    getBlacklistedJwts() {
        const { blacklistedJwts } = this;

        return new Promise((resolve) => {
            resolve(blacklistedJwts);
        });
    }

    isJwtKnown(jwt) {
        const { knownJwts } = this;

        return new Promise((resolve, reject) => {
            if (knownJwts.has(jwt)) {
                resolve();
            } else {
                reject();
            }
        });
    }

    isJwtBlacklisted(jwt) {
        const { blacklistedJwts } = this;

        return new Promise((resolve, reject) => {
            if (blacklistedJwts.has(jwt)) {
                resolve();
            } else {
                reject();
            }
        });
    }

    addKnownJwt(jwt) {
        const { JwtModel, knownJwts } = this;

        return new Promise((resolve) => {
            knownJwts.set(jwt, new JwtModel(jwt));
            resolve();
        });
    }

    addBlacklistedJwt(jwt) {
        const { JwtModel, blacklistedJwts } = this;

        return new Promise((resolve) => {
            blacklistedJwts.set(jwt, new JwtModel(jwt));
            resolve();
        });
    }

    removeKnownJwt(jwt) {
        const { knownJwts } = this;

        return new Promise((resolve) => {
            knownJwts.delete(jwt);
            resolve();
        });
    }

    removeBlacklistedJwt(jwt) {
        const { blacklistedJwts } = this;

        return new Promise((resolve) => {
            blacklistedJwts.delete(jwt);
            resolve();
        });
    }
};
