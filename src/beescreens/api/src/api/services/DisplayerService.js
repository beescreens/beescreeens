module.exports = class {
    constructor(debug, DisplayerModel, Status) {
        const availableDisplayers = new Map();
        const unavailableDisplayers = new Map();

        this.debug = debug('displayer-service');
        this.DisplayerModel = DisplayerModel;
        this.Status = Status;
        this.availableDisplayers = availableDisplayers;
        this.unavailableDisplayers = unavailableDisplayers;
    }

    registerDisplayer(name, location, width, height) {
        const {
            debug,
            DisplayerModel,
            Status,
            availableDisplayers,
        } = this;

        return new Promise((resolve) => {
            const displayer = new DisplayerModel(
                null,
                name,
                location,
                width,
                height,
                Status.ACTIVE.toString(),
            );

            availableDisplayers.set(name, displayer);

            debug(`The dipslayer ${name} has successfully been registered.`);

            resolve();
        });
    }

    deleteDisplayer(displayerName) {
        const { availableDisplayers, unavailableDisplayers } = this;

        return new Promise((resolve) => {
            availableDisplayers.delete(displayerName);
            unavailableDisplayers.delete(displayerName);

            resolve();
        });
    }

    getAvailableDisplayers() {
        const { availableDisplayers } = this;

        return new Promise(resolve => resolve(availableDisplayers));
    }

    getAvailableDisplayer() {
        const { Status, availableDisplayers, unavailableDisplayers } = this;

        return new Promise((resolve, reject) => {
            if (availableDisplayers.size > 0) {
                const displayer = availableDisplayers.values().next().value;

                displayer.status = Status.ACTIVE.toString();

                this.deleteDisplayer(displayer.name);

                unavailableDisplayers.set(displayer.name, displayer);

                resolve(displayer);
            } else {
                reject();
            }
        });
    }

    getDisplayer(displayerName) {
        const { availableDisplayers, unavailableDisplayers } = this;

        return new Promise((resolve, reject) => {
            const displayers = new Map([...availableDisplayers, ...unavailableDisplayers]);

            const displayer = displayers.get(displayerName);

            if (displayer != null) {
                resolve(displayer);
            } else {
                reject();
            }
        });
    }

    getDisplayers() {
        const { availableDisplayers, unavailableDisplayers } = this;

        return new Promise((resolve) => {
            resolve([...new Map([...availableDisplayers, ...unavailableDisplayers]).values()]);
        });
    }


    restituteDisplayer(displayerName) {
        const {
            debug,
            Status,
            availableDisplayers,
        } = this;

        return new Promise((resolve, reject) => {
            this.getDisplayer(displayerName)
                .then((displayer) => {
                    const { name } = displayer;
                    // eslint-disable-next-line no-param-reassign
                    displayer.status = Status.INACTIVE.toString();

                    this.deleteDisplayer(name);

                    availableDisplayers.set(displayer.name, displayer);

                    debug(`Displayer ${name} has been successfully restituted.`);
                    resolve();
                })
                .catch(() => reject());
        });
    }

    /**
     * This function update the status of a displayer in case an administrator
     * want to restain the acces of a displayer
     *
     * @param {String} displayerName The displayer's name
     * @param {String} status The updated displayer's status
     */
    updateDisplayer(displayerName, width, height, status) {
        const { Status, availableDisplayers, unavailableDisplayers } = this;

        return new Promise((resolve, reject) => {
            this.getDisplayer(displayerName)
                .then((displayer) => {
                    this.deleteDisplayer(displayer.name);

                    // eslint-disable-next-line no-param-reassign
                    displayer.width = width;

                    // eslint-disable-next-line no-param-reassign
                    displayer.height = height;

                    switch (status) {
                    case Status.ACTIVE.toString():
                        // eslint-disable-next-line no-param-reassign
                        displayer.status = Status.INACTIVE.toString();
                        availableDisplayers.set(displayer.name, displayer);
                        resolve();
                        break;
                    case Status.INACTIVE.toString():
                        // eslint-disable-next-line no-param-reassign
                        displayer.status = Status.ACTIVE.toString();
                        unavailableDisplayers.set(displayer.name, displayer);
                        resolve();
                        break;
                    default:
                        reject();
                        break;
                    }
                })
                .catch(() => {
                    reject();
                });
        });
    }
};
