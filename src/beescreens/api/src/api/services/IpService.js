module.exports = class {
    constructor(IpModel) {
        this.IpModel = IpModel;

        const knownIps = new Map();
        const blacklistedIps = new Map();

        this.knownIps = knownIps;
        this.blacklistedIps = blacklistedIps;
    }

    getKnownIps() {
        const { knownIps } = this;

        return new Promise((resolve) => {
            resolve(knownIps);
        });
    }

    getBlacklistedIps() {
        const { blacklistedIps } = this;

        return new Promise((resolve) => {
            resolve(blacklistedIps);
        });
    }

    isIpKnown(ip) {
        const { knownIps } = this;

        return new Promise((resolve, reject) => {
            if (knownIps.has(ip)) {
                resolve();
            } else {
                reject();
            }
        });
    }

    isIpBlacklisted(ip) {
        const { blacklistedIps } = this;

        return new Promise((resolve, reject) => {
            if (blacklistedIps.has(ip)) {
                resolve();
            } else {
                reject();
            }
        });
    }

    addKnownIp(ip) {
        const { IpModel, knownIps } = this;

        return new Promise((resolve) => {
            knownIps.set(ip, new IpModel(ip));
            resolve();
        });
    }

    addBlacklistedIp(ip) {
        const { IpModel, blacklistedIps } = this;

        return new Promise((resolve) => {
            blacklistedIps.set(ip, new IpModel(ip));
            resolve();
        });
    }

    removeKnownIp(ip) {
        const { knownIps } = this;

        return new Promise((resolve) => {
            knownIps.delete(ip);
            resolve();
        });
    }

    removeBlacklistedIp(ip) {
        const { blacklistedIps } = this;

        return new Promise((resolve) => {
            blacklistedIps.delete(ip);
            resolve();
        });
    }
};
