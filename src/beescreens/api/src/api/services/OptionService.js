module.exports = class {
    constructor(
        OptionModel,
        displayersPassword,
    ) {
        const options = new Map();

        [
            // Displayers options
            new OptionModel(
                'accept_new_displayers',
                'Accept new displayers',
                'Accept new displayers to connect on the server',
                true,
            ),
            new OptionModel(
                'accept_known_displayers',
                'Accept known displayers',
                'Accept known displayers to connect on the server',
                true,
            ),
            new OptionModel(
                'displayers_password',
                'Displayers password',
                'The password asked to be accepted as a new displayer (empty means no password is needed)',
                displayersPassword,
            ),
            // Players options
            new OptionModel(
                'accept_new_players',
                'Accept new players',
                'Accept new players to connect on the server',
                true,
            ),
        ].forEach((option) => {
            options.set(option.id, option);
        });

        this.OptionModel = OptionModel;
        this.options = options;
    }

    changeOptionValue(optionId, value) {
        const { options } = this;

        return new Promise((resolve, reject) => {
            const option = options.get(optionId);

            if (option != null) {
                option.value = value;

                options.set(optionId, option);

                resolve();
            } else {
                reject();
            }
        });
    }

    getOptionValue(optionId) {
        const { options } = this;

        return options.get(optionId).value;
    }

    getOptions() {
        const { options } = this;

        return new Promise((resolve) => {
            resolve([...options.values()]);
        });
    }
};
