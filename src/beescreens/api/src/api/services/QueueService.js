// TODO - is this good practice in DI
const FastPriorityQueue = require('fastpriorityqueue');

module.exports = class {
    constructor(QueueElementModel) {
        const queue = new FastPriorityQueue((e1, e2) => e1.position < e2.position);

        this.QueueElementModel = QueueElementModel;
        this.queue = queue;
        this.nextPosition = 1;
    }

    notifyPositionInQueue() {
        const { queue } = this;

        return new Promise((resolve) => {
            const queueTotal = queue.size;

            let currentPosition = 1;

            queue.forEach((queueElement) => {
                queueElement.player.send('queue', {
                    queuePosition: currentPosition,
                    queueTotal,
                });

                currentPosition += 1;
            });

            resolve();
        });
    }

    addPlayerProcess(player, app) {
        const { nextPosition } = this;

        return new Promise((resolve) => {
            this.addSystemProcess(player, app, nextPosition)
                .then((position) => {
                    this.nextPosition += 1;

                    resolve(position);
                });
        });
    }

    addSystemProcess(appUser, app, position) {
        const { QueueElementModel, queue } = this;

        return new Promise((resolve) => {
            queue.add(new QueueElementModel(appUser, app, position));

            this.notifyPositionInQueue()
                .then(() => {
                    resolve(position);
                });
        });
    }

    getNext() {
        const { queue } = this;

        return new Promise((resolve) => {
            const queueElement = queue.poll();

            this.notifyPositionInQueue()
                .then(() => resolve(queueElement));
        });
    }

    deleteQueueElement(position) {
        const { queue } = this;

        return new Promise((resolve) => {
            queue.remove({
                position,
            });

            this.notifyPositionInQueue()
                .then(() => resolve());
        });
    }

    updateQueueElement(position, newPosition) {
        const { queue } = this;

        return new Promise((resolve, reject) => {
            const queueElement = queue.remove(position);

            queueElement.position = newPosition;

            if (queueElement != null) {
                queue.add(queueElement);

                resolve();
            } else {
                reject();
            }
        });
    }

    getQueueElement(position) {
        const { queue } = this;

        return new Promise((resolve, reject) => {
            const queueElement = queue.remove(position);

            if (queueElement != null) {
                queue.add(queueElement);

                resolve(queueElement);
            } else {
                reject();
            }
        });
    }

    getQueueElements() {
        const { queue } = this;

        return new Promise((resolve) => {
            const queueElements = [];

            queue.forEach((queueElement) => {
                queueElements.push(queueElement);
            });

            resolve(queueElements);
        });
    }
};
