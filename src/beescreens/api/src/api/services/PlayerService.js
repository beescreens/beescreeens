
module.exports = class {
    constructor(PlayerModel, Status) {
        const players = new Map();

        this.PlayerModel = PlayerModel;
        this.Status = Status;
        this.players = players;
    }

    addPlayer(username) {
        const { players, PlayerModel, Status } = this;

        return new Promise((resolve) => {
            const player = new PlayerModel(null, username, Status.ACTIVE.toString());

            players.set(player.username, player);

            resolve(player);
        });
    }

    getPlayer(username) {
        const { players } = this;

        return new Promise((resolve, reject) => {
            const player = players.get(username);

            if (player != null) {
                resolve(player);
            } else {
                reject();
            }
        });
    }

    getPlayers() {
        const { players } = this;

        return new Promise((resolve) => {
            resolve([...players.values()]);
        });
    }

    kickPlayer(username) {
        // TODO - Use QueueService and/or SessionService to kick the player
        const { players } = this;

        return new Promise((resolve) => {
            players.delete(username);
            resolve();
        });
    }

    updatePlayer(username, status) {
        const { players } = this;

        return new Promise((resolve, reject) => {
            const player = players.get(username);

            if (player != null) {
                player.status = status;

                players.set(username, player);

                resolve();
            } else {
                reject();
            }
        });
    }
};
