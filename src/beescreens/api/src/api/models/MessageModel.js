class MessageModel {
    constructor(
        title,
        content,
        level,
    ) {
        this.title = title;
        this.content = content;
        this.level = level;
    }
}

module.exports = () => MessageModel;
