class QueueElementModel {
    constructor(player, application, position) {
        this.player = player;
        this.application = application;
        this.position = position;
    }
}

module.exports = () => QueueElementModel;
