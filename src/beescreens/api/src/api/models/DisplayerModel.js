class DisplayerModel {
    constructor(
        socket,
        name,
        location,
        width,
        height,
        status,
    ) {
        this.socket = socket;
        this.name = name;
        this.location = location;
        this.width = width;
        this.height = height;
        this.status = status;
    }

    send(messageType, data) {
        const { socket } = this;

        return new Promise((resolve, reject) => {
            if (socket != null) {
                socket.emit(messageType, data);
                resolve();
            } else {
                reject();
            }
        });
    }
}

module.exports = () => DisplayerModel;
