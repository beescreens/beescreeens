class SessionModel {
    constructor(id, player, application, displayer) {
        this.id = id;
        this.player = player;
        this.application = application;
        this.displayer = displayer;
    }
}

module.exports = () => SessionModel;
