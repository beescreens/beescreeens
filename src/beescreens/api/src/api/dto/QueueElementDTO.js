class QueueElementDTO {
    constructor(queueElement) {
        this.player = queueElement.player;
        this.position = queueElement.position;
        this.app = queueElement.app;
    }
}

module.exports = () => QueueElementDTO;
