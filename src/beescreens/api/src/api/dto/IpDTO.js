class IpDTO {
    constructor(ip) {
        this.ip = ip.ip;
        this.timestamp = ip.timestamp;
    }
}

module.exports = () => IpDTO;
