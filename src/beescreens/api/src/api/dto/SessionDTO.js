class SessionDTO {
    constructor(session) {
        this.id = session.id;
        this.player = session.player;
        this.app = session.app;
        this.displayer = session.displayer;
    }
}

module.exports = () => SessionDTO;
