class MessageDTO {
    constructor(message) {
        this.title = message.title;
        this.content = message.content;
        this.level = message.level;
    }
}

module.exports = () => MessageDTO;
