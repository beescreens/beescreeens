class UserDTO {
    constructor(user) {
        this.username = user.username;
        this.role = user.role;
        this.status = user.status;
    }
}

module.exports = () => UserDTO;
