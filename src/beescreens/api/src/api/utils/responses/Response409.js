module.exports = class {
    constructor() {
        this.code = 409;
    }

    send(res) {
        const { code } = this;

        res.status(code);

        res.end();
    }
};
