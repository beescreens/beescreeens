
module.exports = ({ JwtService }) => {
    function verify(req, authOrSecDef, bearer, next) {
        Promise.resolve(() => {
            // Get scopes from the request
            const scopes = req.swagger.operation['x-security-scopes'];

            // Check if the Bearer is correctly formated
            if (bearer && bearer.indexOf('Bearer ') === 0) {
                const token = bearer.replace('Bearer ', '');

                JwtService.isJwtKnown(token)
                    .then(() => {
                        req.jwtWasKnown = true;
                    })
                    .catch(() => {
                        req.jwtWasKnown = false;
                    })
                    .finally(() => JwtService.verify(token))
                    .then((decodedToken) => {
                        // Check if the scopes are correctly formated and decoded token has role
                        if (Array.isArray(scopes) && decodedToken.role) {
                            const rolesMatch = scopes.indexOf(decodedToken.role) !== -1;

                            if (rolesMatch) {
                                req.auth = decodedToken;
                            } else {
                                throw new Error();
                            }
                        }
                    })
                    .catch(() => {
                        throw new Error();
                    });
            } else {
                throw new Error();
            }
        })
            .then(() => next())
            .catch(() => req.res.status(403).json({ message: 'Error: Access Denied' }));
    }

    return {
        verify,
    };
};
