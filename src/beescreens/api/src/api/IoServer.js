const EventEmitter = require('events');
const Io = require('socket.io');
const IoAuth = require('socketio-auth');

module.exports = class extends EventEmitter {
    /**
     * The constructor.
     */
    constructor(
        ApplicationService,
        debug,
        DisplayerService,
        EntityTypes,
        JwtService,
        PlayService,
        PlayerService,
        SessionService,
        restServer,
        Status,
    ) {
        super();

        this.ApplicationService = ApplicationService;
        this.debug = debug('beescreens:io-server');
        this.DisplayerService = DisplayerService;
        this.EntityTypes = EntityTypes;
        this.JwtService = JwtService;
        this.PlayService = PlayService;
        this.PlayerService = PlayerService;
        this.SessionService = SessionService;
        this.restServer = restServer;
        this.Status = Status;
    }

    /**
     * Start the server.
     */
    start() {
        const {
            ApplicationService,
            debug,
            DisplayerService,
            EntityTypes,
            JwtService,
            PlayService,
            PlayerService,
            restServer,
            SessionService,
            Status,
        } = this;

        const io = Io(restServer.instance);

        const playersEndpoint = io.of('/api/players');
        const displayersEndpoint = io.of('/api/displayers');

        const authenticatePlayer = (socket, data, callback) => {
            const { jwt, params } = data;

            JwtService.verify(jwt)
                .then((decodedToken) => {
                    const isAuthorized = decodedToken.role != null
                        && decodedToken.role === EntityTypes.PLAYER.toString();

                    if (isAuthorized) {
                        const { username, applicationName } = decodedToken;

                        let player;
                        let application;

                        Promise.all([
                            PlayerService.getPlayer(username).then((foundPlayer) => {
                                player = foundPlayer;

                                // eslint-disable-next-line no-param-reassign
                                foundPlayer.socket = socket;

                                // eslint-disable-next-line no-param-reassign
                                player.params = params;
                            }),
                            ApplicationService.getApp(applicationName).then((foundApp) => {
                                if (foundApp.status === Status.ACTIVE.toString()) {
                                    application = foundApp;
                                } else {
                                    throw new Error();
                                }
                            }),
                        ])
                            .then(() => PlayService.joinQueue(player, application))
                            .then((position) => {
                                debug(`Player '${username}' joined the queue to play '${applicationName}'.`);

                                player.position = position;

                                socket.on('disconnect', () => {
                                    PlayService.playerDisconnect(player)
                                        .then(() => {
                                            debug(`Player '${username}' has exited the queue.`);
                                            SessionService.canStartSession().catch(() => {});
                                        });
                                });

                                SessionService.canStartSession().catch(() => {});
                                callback(null, true);
                            })
                            .catch(() => callback(null, false));
                    }
                }).catch(() => {
                    debug('Player not authorized to access /api/players');
                    callback(null, false);
                });
        };

        const authenticateDisplayer = (socket, data, callback) => {
            const { jwt } = data;

            JwtService.verify(jwt)
                .then((decodedToken) => {
                    const isAuthorized = decodedToken.role != null
                        && decodedToken.role === EntityTypes.DISPLAYER.toString();

                    if (isAuthorized) {
                        const { displayerName } = decodedToken;

                        DisplayerService.getDisplayer(displayerName)
                            .then((displayer) => {
                                debug(`Displayer '${displayerName}' connected.`);

                                // eslint-disable-next-line no-param-reassign
                                displayer.socket = socket;

                                socket.on('disconnect', () => {
                                    PlayService.displayerDisconnect(displayer)
                                        .then(() => {
                                            debug(`Displayer '${displayerName}' has exited the pool.`);
                                        });
                                });

                                SessionService.canStartSession().catch(() => {});
                                callback(null, true);
                            })
                            .catch(() => callback(null, false));
                    }
                })
                .catch(() => {
                    debug('Displayer not authorized to access /api/displayers');
                    callback(null, false);
                });
        };

        const authorizedPlayer = (player) => {
            player.on('send', (data) => {
                debug(`Data received from player: ${data}`);

                player.emit('receive', 'receive');
            });
        };

        const authorizedDisplayer = () => {
            // Nothing more
        };

        const playerDisconnects = () => {
        };

        const displayerDisconnects = () => {
            debug('Displayer has disconnected');
        };

        IoAuth(playersEndpoint, {
            authenticate: authenticatePlayer,
            postAuthenticate: authorizedPlayer,
            disconnect: playerDisconnects,
            timeout: 1000,
        });

        IoAuth(displayersEndpoint, {
            authenticate: authenticateDisplayer,
            postAuthenticate: authorizedDisplayer,
            disconnect: displayerDisconnects,
            timeout: 1000,
        });

        debug('Ready for connections');
        this.emit('started');
    }

    /**
     * Stop the server.
     */
    stop() {
        const { debug } = this;

        debug('Server gracefully stopped.');
        this.emit('stopped');
    }
};
