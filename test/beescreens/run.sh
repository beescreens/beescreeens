#!/usr/bin/env bash
######################################################
# Name:             run.sh
# Author:           BeeScreens
# Creation:         29.06.2015
# Description:      Start unit testing when ressource available.
######################################################
./wait-for-it.sh ${BEESCREENS_HOSTNAME}
./wait-for-it.sh ${DRAWING_APP_SERVER_HOSTAME}

startTest() {
    directory=${1}
    root=${PWD}

    cd ${directory}
    npm start
    cd ${root}
}

if [[ $? == 0 ]]; then
    startTest "api"
    # startTest "display"
    # startTest "play"
    # startTest "admin"
fi

exit $?
