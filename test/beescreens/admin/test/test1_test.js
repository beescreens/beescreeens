
Feature('Admin');

Scenario('Login', (I) => {

  I.amOnPage('http://localhost:8081/#/login');
  I.see('Login');

  I.fillField('username', 'admin');
  I.fillField('password', 'admin');

  I.click('Login');

  I.amOnPage('http://localhost:8081/#/Home');

  I.click(locate('a').withAttr({ href: '#/Users' }));
  
  I.amOnPage('http://localhost:8081/#/Users');

  I.waitForElement('#addBtn', 10); // secs
  
  I.click('#addBtn');

  I.fillField('#username', 'test');
  I.fillField('#password', 'test');
  I.fillField('#confirmPassword', 'test');
  I.fillField('#role', 'admin');
  
  I.click('#btnCreate');
  I.wait(5);
  I.seeInPopup('Status 204');
  
  
});
