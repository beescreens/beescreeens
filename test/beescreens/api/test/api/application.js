const axios = require('axios');
const container = require('../container');

describe('Applications', () => {
    const {
        beescreensHostname,
        adminUsername,
        adminPassword,
    } = container.cradle;

    const url = `http://${beescreensHostname}/api`;

    const client = axios.create({
        baseURL: url,
        timeout: 2000,
    });

    const admin = {
        username: adminUsername,
        password: adminPassword,
    };


    // let adminToken = null;
    // let headersToken = null;
    // let applicationName = null;

    describe('As an admin...', () => {
        it('...I can login', async () => {
            await client.post('/login', admin)
                .then((res) => {
                    res.status.should.equal(200);

                    return res;
                })
                .then((body) => {
                    body.data.jwt.should.exist();
                    /*
                    adminToken = body.data.jwt;
                    headersToken = {
                        headers: {
                            Authorization: `Bearer ${adminToken}`,
                        },
                    };
                    */
                });
        });

        it('...I can get all the applications', async () => {
            /*
            await client.get('/apps', null, headersToken)
                .then((res) => {
                    res.status.should.equal(200);

                    return res;
                })
                .then((res) => {
                    res.data.length.should.equal(1);
                    applicationName = res.data[0].name;
                });
            */
        });

        it('...I can retrieve the first application', async () => {
            /*
            await client.get(`/apps/${applicationName}`, null, headersToken)
                .then((res) => {
                    res.status.should.equal(200);

                    return res;
                })
                .then((res) => {
                    res.data.name.should.equal(applicationName);
                });
            */
        });

        it('...I can change the status of the first application', async () => {
            /*
            const status = 'inactive';
            const body = {
                status,
            };
            // first, we change the status of the application
            await client.patch(`/apps/${applicationName}`, body, headersToken)
                .then(async (res) => {
                    res.status.should.equal(204);

                    // second, we test if the status was changed
                    return client.get(`/apps/${applicationName}`, null, headersToken);
                })
                .then((getRes) => {
                    getRes.status.should.equal(200);

                    return getRes;
                })
                .then((getRes) => {
                    getRes.data.status.should.equal(status);
                });
            */
        });
    });
});
