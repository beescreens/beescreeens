const fetch = require('node-fetch');

const container = require('../container');

describe('User', () => {
    const {
        beescreensHostname,
        adminUsername,
        adminPassword,
    } = container.cradle;

    const url = `http://${beescreensHostname}/api`;

    const admin = {
        username: adminUsername,
        password: adminPassword,
    };

    const user = {
        username: 'asdfasdf',
        password: 'withAPassword',
        role: 'moderator',
    };

    let token = null;

    it('Can login', async () => {
        await fetch(`${url}/login`, {
            method: 'post',
            body: JSON.stringify(admin),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((res) => {
                res.status.should.equal(200);

                return res.json();
            })
            .then((body) => {
                body.jwt.should.exist();
                token = body.jwt;
            });
    });

    it('Can create a new user', async () => {
        const result = await fetch(`${url}/users`, {
            method: 'post',
            body: JSON.stringify(user),
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        result.status.should.equal(204);
    });

    it('Can get the created user', async () => {
        await fetch(`${url}/users/${user.username}`, {
            method: 'get',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
            .then(res => res.json())
            .then((json) => {
                json.username.should.equal(user.username);
            });
    });

    it('Can delete the created user', async () => {
        const result = await fetch(`${url}/users/${user.username}`, {
            method: 'delete',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        result.status.should.equal(204);
    });
    /* We actually don't send a 404 if we don't retrieve it
    it('Trying to get the created user after deletation should return 500', async () => {
        const result = await fetch(`${url}/users/${user.username}`, {
            method: 'get',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        result.status.should.equal(404);
    }); */
});
