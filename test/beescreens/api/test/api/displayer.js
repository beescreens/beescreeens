const fetch = require('node-fetch');

const container = require('../container');

describe('Displayer', () => {
    const {
        beescreensHostname,
        adminUsername,
        adminPassword,
        displayersPassword,
        expect,
    } = container.cradle;

    const url = `http://${beescreensHostname}/api`;

    const admin = {
        username: adminUsername,
        password: adminPassword,
    };

    const displayer1 = {
        name: 'G01-01',
        location: 'Principal tour',
        width: 1024,
        height: 768,
    };

    const displayer2 = {
        name: 'G02-01',
        location: 'Principal tour',
        width: 1024,
        height: 768,
    };

    const displayer3 = {
        name: 'G03-01',
        location: 'Principal tour',
        width: 1024,
        height: 768,
    };

    const displayer4 = {
        name: 'G04-1',
        location: 'Principal tour',
        width: 1024,
        height: 768,
        password: 'WRONG_PASSWORD',
    };

    const displayer5 = {
        name: 'G05-1',
        location: 'Principal tour',
        width: 1024,
        height: 768,
        password: 'RIGHT_PASSWORD',
    };

    const displayer6 = {
        name: 'G06-1',
        location: 'Principal tour',
        width: 1024,
        height: 768,
    };

    let displayerToken = null;
    let adminToken = null;

    describe('As an admin...', () => {
        it('...I can login', async () => {
            await fetch(`${url}/login`, {
                method: 'post',
                body: JSON.stringify(admin),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(200);

                    return res.json();
                })
                .then((body) => {
                    body.jwt.should.exist();
                    adminToken = body.jwt;
                });
        });
    });

    describe('New displayers: ON | Known displayers: ON | Password: OFF', () => {
        it('As a displayer, I can register as a new displayer', async () => {
            await fetch(`${url}/displayers`, {
                method: 'post',
                body: JSON.stringify(displayer1),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(201);

                    return res.json();
                })
                .then((body) => {
                    body.jwt.should.exist();
                    displayerToken = body.jwt;
                });
        });

        it('As a displayer, I can come back as a known displayer', async () => {
            await fetch(`${url}/displayers`, {
                method: 'post',
                body: JSON.stringify(displayer1),
                headers: {
                    Authorization: `Bearer ${displayerToken}`,
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(204);
                });
        });
    });

    describe('New displayers: OFF | Known displayers: ON | Password: OFF', () => {
        it('As a displayer, I cannot register as a new displayer...', async () => {
            await fetch(`${url}/options/accept_new_displayers`, {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    value: 'false',
                }),
            })
                .then(() => fetch(`${url}/displayers`, {
                    method: 'post',
                    body: JSON.stringify(displayer2),
                    headers: {
                        Authorization: `Bearer ${displayerToken}`,
                        'Content-Type': 'application/json',
                    },
                }))
                .then((res) => {
                    res.status.should.equal(403);
                });
        });

        it('...but can come back as a known displayer', async () => {
            await fetch(`${url}/displayers`, {
                method: 'post',
                body: JSON.stringify(displayer1),
                headers: {
                    Authorization: `Bearer ${displayerToken}`,
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(204);
                })
                .then(() => {
                    fetch(`${url}/options/accept_new_displayers`, {
                        method: 'patch',
                        headers: {
                            Authorization: `Bearer ${adminToken}`,
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            value: 'true',
                        }),
                    });
                });
        });
    });

    describe('New displayers: ON | Known displayers: OFF | Password: OFF', () => {
        it('As a displayer, I cannot come back as a known displayer...', async () => {
            await fetch(`${url}/options/accept_known_displayers`, {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    value: 'false',
                }),
            })
                .then(() => fetch(`${url}/displayers`, {
                    method: 'post',
                    body: JSON.stringify(displayer1),
                    headers: {
                        Authorization: `Bearer ${displayerToken}`,
                        'Content-Type': 'application/json',
                    },
                }))
                .then((res) => {
                    res.status.should.equal(403);
                });
        });

        it('...but can register as a new displayer', async () => {
            await fetch(`${url}/options/accept_known_displayers`, {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    value: 'true',
                }),
            })
                .then(() => fetch(`${url}/displayers`, {
                    method: 'post',
                    body: JSON.stringify(displayer2),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }))
                .then((res) => {
                    res.status.should.equal(201);
                });
        });
    });

    describe('New displayers: ON | Known displayers: ON | Password: ON', () => {
        it('As a displayer, I cannot register without a required password', async () => {
            await fetch(`${url}/options/displayers_password`, {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    value: 'RIGHT_PASSWORD',
                }),
            })
                .then(() => fetch(`${url}/displayers`, {
                    method: 'post',
                    body: JSON.stringify(displayer3),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }))
                .then((res) => {
                    res.status.should.equal(403);
                });
        });

        it('As a displayer, I cannot register as a displayer with a wrong password', async () => {
            await fetch(`${url}/displayers`, {
                method: 'post',
                body: JSON.stringify(displayer4),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(403);
                });
        });

        it('As a displayer, I can register as a displayer with the right password', async () => {
            await fetch(`${url}/displayers`, {
                method: 'post',
                body: JSON.stringify(displayer5),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(201);
                });
        });

        it('As a displayer, I can come back as a known displayer', async () => {
            await fetch(`${url}/displayers`, {
                method: 'post',
                body: JSON.stringify(displayer1),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(204);

                    return fetch(`${url}/options/displayers_password`, {
                        method: 'patch',
                        headers: {
                            Authorization: `Bearer ${adminToken}`,
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            value: displayersPassword,
                        }),
                    });
                });
        });
    });

    describe('New displayers: OFF | Known displayers: OFF | Password: ON/OFF', () => {
        it('As a displayer, I cannot register as a new displayer', async () => {
            await fetch(`${url}/options/accept_new_displayers`, {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    value: 'false',
                }),
            })
                .then(() => fetch(`${url}/options/accept_known_displayers`, {
                    method: 'patch',
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        value: 'false',
                    }),
                }))
                .then(() => fetch(`${url}/displayers`, {
                    method: 'post',
                    body: JSON.stringify(displayer6),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }))
                .then((res) => {
                    res.status.should.equal(403);
                });
        });

        it('As a displayer, I cannot register as a known displayer', async () => {
            await fetch(`${url}/displayers`, {
                method: 'post',
                body: JSON.stringify(displayer1),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(403);

                    return fetch(`${url}/options/accept_new_displayers`, {
                        method: 'patch',
                        headers: {
                            Authorization: `Bearer ${adminToken}`,
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            value: 'true',
                        }),
                    });
                })
                .then(() => fetch(`${url}/options/accept_known_displayers`, {
                    method: 'patch',
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        value: 'true',
                    }),
                }));
        });
    });

    describe('As an admin...', () => {
        it('...I can get all displayers', async () => {
            await fetch(`${url}/displayers`, {
                method: 'get',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                },
            })
                .then((res) => {
                    res.status.should.equal(200);

                    return res.json();
                })
                .then((displayers) => {
                    expect(displayers).to.have.lengthOf(3);

                    displayers.forEach((displayer) => {
                        expect(displayer).to.have.property('name');
                        expect(displayer).to.have.property('location');
                        expect(displayer).to.have.property('width');
                        expect(displayer).to.have.property('height');
                        expect(displayer).to.have.property('status');
                    });

                    expect(displayers.find(o => o.name === 'G01-01')).to.not.be.a('null');
                    expect(displayers.find(o => o.name === 'G02-01')).to.not.be.a('null');
                    expect(displayers.find(o => o.name === 'G05-01')).to.not.be.a('null');
                });
        });

        it('...I can get a displayer', async () => {
            await fetch(`${url}/displayers/${displayer1.name}`, {
                method: 'get',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                },
            })
                .then((res) => {
                    res.status.should.equal(200);

                    return res.json();
                })
                .then((json) => {
                    json.name.should.equal(displayer1.name);
                });
        });
        /*
        it('...I can update a displayer', async () => {
            displayer1.status = 'inactive';

            await fetch(`${url}/displayers/${displayer1.name}`, {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(displayer1),
            })
                .then((res) => {
                    res.status.should.equal(204);

                    return fetch(`${url}/displayers/${displayer1.name}`, {
                        method: 'get',
                        headers: {
                            Authorization: `Bearer ${adminToken}`,
                        },
                    });
                })
                .then(res => res.json())
                .then((json) => {
                    json.status.should.equal('inactive');
                });
        }); */

        it('...I can delete a displayer', async () => {
            const result = await fetch(`${url}/displayers/${displayer1.name}`, {
                method: 'delete',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                },
            });

            result.status.should.equal(204);
        });
    });
});
