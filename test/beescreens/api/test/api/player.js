/*
const fetch = require('node-fetch');

const container = require('../container');

describe('Player', () => {
    const {
        beescreensHostname,
        adminUsername,
        adminPassword,
        playersPassword,
        expect,
    } = container.cradle;

    const url = `http://${beescreensHostname}/api`;

    const admin = {
        username: adminUsername,
        password: adminPassword,
    };

    const player1 = {
        username: 'P1',
    };

    const player2 = {
        username: 'P2',
    };

    const player3 = {
        username: 'P3',
    };

    const player4 = {
        username: 'P4',
        password: 'WRONG_PASSWORD',
    };

    const player5 = {
        username: 'P5',
        password: 'RIGHT_PASSWORD',
    };

    const player6 = {
        username: 'P6',
    };

    let playerToken = null;
    let adminToken = null;

    describe('As an admin...', () => {
        it('...I can login', async () => {
            await fetch(`${url}/login`, {
                method: 'post',
                body: JSON.stringify(admin),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(200);

                    return res.json();
                })
                .then((body) => {
                    body.jwt.should.exist();
                    adminToken = body.jwt;
                });
        });
    });

    describe('New players: ON | Known players: ON | Password: OFF', () => {
        it('As a player, I can register as a new player', async () => {
            await fetch(`${url}/players`, {
                method: 'post',
                body: JSON.stringify(player1),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(201);

                    return res.json();
                })
                .then((body) => {
                    body.jwt.should.exist();
                    playerToken = body.jwt;
                });
        });

        it('As a player, I can come back as a known player', async () => {
            await fetch(`${url}/players`, {
                method: 'post',
                body: JSON.stringify(player1),
                headers: {
                    Authorization: `Bearer ${playerToken}`,
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(200);
                });
        });
    });

    describe('New players: OFF | Known players: ON | Password: OFF', () => {
        it('As a player, I cannot register as a new player...', async () => {
            await fetch(`${url}/options/accept_new_players`, {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    value: 'false',
                }),
            })
                .then(() => fetch(`${url}/players`, {
                    method: 'post',
                    body: JSON.stringify(player2),
                    headers: {
                        Authorization: `Bearer ${playerToken}`,
                        'Content-Type': 'application/json',
                    },
                }))
                .then((res) => {
                    res.status.should.equal(403);
                });
        });

        it('...but can come back as a known player', async () => {
            await fetch(`${url}/players`, {
                method: 'post',
                body: JSON.stringify(player1),
                headers: {
                    Authorization: `Bearer ${playerToken}`,
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(200);
                })
                .then(() => {
                    fetch(`${url}/options/accept_new_players`, {
                        method: 'patch',
                        headers: {
                            Authorization: `Bearer ${adminToken}`,
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            value: 'true',
                        }),
                    });
                });
        });
    });

    describe('New players: ON | Known players: OFF | Password: OFF', () => {
        it('As a player, I cannot come back as a known player...', async () => {
            await fetch(`${url}/options/accept_known_players`, {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    value: 'false',
                }),
            })
                .then(() => fetch(`${url}/players`, {
                    method: 'post',
                    body: JSON.stringify(player1),
                    headers: {
                        Authorization: `Bearer ${playerToken}`,
                        'Content-Type': 'application/json',
                    },
                }))
                .then((res) => {
                    res.status.should.equal(403);
                });
        });

        it('...but can register as a new player', async () => {
            await fetch(`${url}/options/accept_known_players`, {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    value: 'true',
                }),
            })
                .then(() => fetch(`${url}/players`, {
                    method: 'post',
                    body: JSON.stringify(player2),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }))
                .then((res) => {
                    res.status.should.equal(201);
                });
        });
    });

    describe('New players: ON | Known players: ON | Password: ON', () => {
        it('As a player, I cannot register without a required password', async () => {
            await fetch(`${url}/options/players_password`, {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    value: 'RIGHT_PASSWORD',
                }),
            })
                .then(() => fetch(`${url}/players`, {
                    method: 'post',
                    body: JSON.stringify(player3),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }))
                .then((res) => {
                    res.status.should.equal(403);
                });
        });

        it('As a player, I cannot register as a player with a wrong password', async () => {
            await fetch(`${url}/players`, {
                method: 'post',
                body: JSON.stringify(player4),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(403);
                });
        });

        it('As a player, I can register as a player with the right password', async () => {
            await fetch(`${url}/players`, {
                method: 'post',
                body: JSON.stringify(player5),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(201);
                });
        });

        it('As a player, I can come back as a known player', async () => {
            await fetch(`${url}/players`, {
                method: 'post',
                body: JSON.stringify(player1),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(200);

                    return fetch(`${url}/options/players_password`, {
                        method: 'patch',
                        headers: {
                            Authorization: `Bearer ${adminToken}`,
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            value: playersPassword,
                        }),
                    });
                });
        });
    });

    describe('New players: OFF | Known players: OFF | Password: ON/OFF', () => {
        it('As a player, I cannot register as a new player', async () => {
            await fetch(`${url}/options/accept_new_players`, {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    value: 'false',
                }),
            })
                .then(() => fetch(`${url}/options/accept_known_players`, {
                    method: 'patch',
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        value: 'false',
                    }),
                }))
                .then(() => fetch(`${url}/players`, {
                    method: 'post',
                    body: JSON.stringify(player6),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }))
                .then((res) => {
                    res.status.should.equal(403);
                });
        });

        it('As a player, I cannot register as a known player', async () => {
            await fetch(`${url}/players`, {
                method: 'post',
                body: JSON.stringify(player1),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => {
                    res.status.should.equal(403);

                    return fetch(`${url}/options/accept_new_players`, {
                        method: 'patch',
                        headers: {
                            Authorization: `Bearer ${adminToken}`,
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            value: 'true',
                        }),
                    });
                })
                .then(() => fetch(`${url}/options/accept_known_players`, {
                    method: 'patch',
                    headers: {
                        Authorization: `Bearer ${adminToken}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        value: 'true',
                    }),
                }));
        });
    });

    describe('As an admin...', () => {
        it('...I can get all players', async () => {
            await fetch(`${url}/players`, {
                method: 'get',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                },
            })
                .then((res) => {
                    res.status.should.equal(200);

                    return res.json();
                })
                .then((players) => {
                    expect(players).to.have.lengthOf(3);

                    players.forEach((player) => {
                        expect(player).to.have.property('username');
                        expect(player).to.have.property('status');
                    });

                    expect(players.find(o => o.name === player1.username)).to.not.be.a('null');
                    expect(players.find(o => o.name === player2.username)).to.not.be.a('null');
                    expect(players.find(o => o.name === player3.username)).to.not.be.a('null');
                });
        });

        it('...I can get a player', async () => {
            await fetch(`${url}/players/${player1.username}`, {
                method: 'get',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                },
            })
                .then((res) => {
                    res.status.should.equal(200);

                    return res.json();
                })
                .then((json) => {
                    json.username.should.equal(player1.username);
                });
        });

        it('...I can update a player', async () => {
            player1.status = 'inactive';

            await fetch(`${url}/players/${player1.username}`, {
                method: 'patch',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(player1),
            })
                .then((res) => {
                    res.status.should.equal(200);

                    return fetch(`${url}/players/${player1.username}`, {
                        method: 'get',
                        headers: {
                            Authorization: `Bearer ${adminToken}`,
                        },
                    });
                })
                .then(res => res.json())
                .then((json) => {
                    json.status.should.equal('inactive');
                });
        });

        it('...I can delete a player', async () => {
            const result = await fetch(`${url}/players/${player1.name}`, {
                method: 'delete',
                headers: {
                    Authorization: `Bearer ${adminToken}`,
                },
            });

            result.status.should.equal(200);
        });
    });
});
*/
